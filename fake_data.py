from navigation import chrome_browser, PageObjects
from file_manipulation import cria_csv, escreve_csv


def fake_data():
    site_data = "https://www.fakenamegenerator.com/gen-random-br-br.php"
    file_path = './assets/fake_data.csv'

    driver = chrome_browser(site_data)

    cria_csv(file_path)
    # logging.debug(f'criado arquivo {file_path}')

    for i in range(5):
        row = (PageObjects.executa_fake_data(driver))
        # screenshot = ps.grab()
        # screenshot.save(f'./logs/screenshots/{i}.png')
        escreve_csv(file_path, row)
    # logging.critical('Executado Fake Data.')


if __name__ == '__main__':
    fake_data()
